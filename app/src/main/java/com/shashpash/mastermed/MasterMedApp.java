package com.shashpash.mastermed;

import android.app.Application;

import com.orhanobut.hawk.Hawk;

/**
 * Created by Shashpash on 04.02.2018.
 */

public class MasterMedApp extends Application {

    @Override public void onCreate() {
        super.onCreate();
        Hawk.init(this).build();
    }
}
