package com.shashpash.mastermed;

/**
 * Created by Shashpash on 24.05.2016.
 */
public class ServiceModel {
    private String title;
    private String price;

    public String getTitle() {
        return title;
    }

    public String getPrice() {
        return price;
    }

    public ServiceModel(String title, String price) {

        this.title = title;
        this.price = price;
    }
}
