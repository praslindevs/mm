package com.shashpash.mastermed;

import android.app.DownloadManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;


import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Shashpash on 16.08.2016.
 */
public class DownloadService extends Service {
    ExecutorService es;
    MyBinder binder = new MyBinder();

    String fileName;
    String url;

    @Override
    public void onCreate() {
        super.onCreate();
        es = Executors.newFixedThreadPool(1);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        fileName = intent.getStringExtra("FileName");
        url = intent.getStringExtra("url");

        DownloaderClass downloaderClass = new DownloaderClass();
        es.execute(downloaderClass);

        boolean check = new File(Environment.getExternalStorageDirectory() + "/MasterMed/" + fileName
                + "-1.xls").delete();

        if (check) {
            new File(Environment.getExternalStorageDirectory()
                    + "/MasterMed/" + fileName +".xls").delete();

            new File(Environment.getExternalStorageDirectory() +
                    "/MasterMed/" + fileName + "-1.xls").renameTo(new File(
                    Environment.getExternalStorageDirectory() + "/MasterMed/" + fileName + "-1.xls"));
        }

        return binder;
    }

    class DownloaderClass implements Runnable {
        @Override
        public void run() {

            String servicestring = Context.DOWNLOAD_SERVICE;
            DownloadManager downloadmanager;
            downloadmanager = (DownloadManager) getSystemService(servicestring);
            Uri downloadUri = Uri
                    .parse(url);
            DownloadManager.Request request = new DownloadManager.Request(
                    downloadUri);

            request.setAllowedNetworkTypes(
                    DownloadManager.Request.NETWORK_WIFI
                            | DownloadManager.Request.NETWORK_MOBILE)
                    .setAllowedOverRoaming(false)
                    .setDestinationInExternalPublicDir("/MasterMed", fileName + ".xls");

            downloadmanager.enqueue(request);

        }
    }

    public class MyBinder extends Binder {
        public DownloadService getService() {
            return DownloadService.this;
        }
    }
}
