package com.shashpash.mastermed;

import android.os.Environment;

import com.shashpash.mastermed.model.MenuEntry;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;

/**
 * Created by Shashpash on 22.06.2016.
 */
public class ParsingExcel {
    private String dbStr;
    private String strHyouji = "";

    private ArrayList<String>[] priceList = createList();
    private String[][] arrays;
    private List<MenuEntry> settingsArray = new ArrayList<>();

    public ParsingExcel(String fileName) {
        dbStr = Environment.getExternalStorageDirectory() + "/MasterMed/" + fileName;
        if (new File(dbStr).exists()) {
            if (fileName.equals("Price.xls")) {
                arrays = readPrice();
            }
            if (fileName.equals("Discounts.xls")) {
                arrays = readDiscounts();
            }
            if (fileName.equals("Settings.xls")) {
                settingsArray = readSettings();
            }

            if (arrays != null) {
                for (String[] array : arrays) {
                    for (int j = 0; j < array.length; j++) {
                        priceList[j].add(array[j]);
                    }
                }
            }
        }
    }

    private List<MenuEntry> readSettings() {
        List<MenuEntry> settings = new ArrayList<>();
        Workbook workbook = null;
        try {
            WorkbookSettings ws = new WorkbookSettings();
            ws.setGCDisabled(true);

            workbook = Workbook.getWorkbook(new File(dbStr), ws);

            Sheet sheet = workbook.getSheet(0);

            int rowCount = sheet.getRows();

            String[][] result = new String[rowCount][];
            for (int i = 0; i < rowCount; i++) {
                Cell[] row = sheet.getRow(i);

                result[i] = new String[row.length];
                for (int j = 0; j < row.length; j++) {
                    result[i][j] = row[j].getContents();
                }
            }
            for (String[] menuEntry : result) {
                settings.add(MenuEntry.fromStringArray(menuEntry));
            }
            return settings;

        } catch (Exception e) {
            strHyouji = strHyouji + e.toString();
        } finally {
            if (workbook != null) {
                workbook.close();
            }
        }
        return settings;
    }

    public String[][] readPrice() {
        Workbook workbook = null;
        try {
            WorkbookSettings ws = new WorkbookSettings();
            ws.setGCDisabled(true);

            workbook = Workbook.getWorkbook(new File(dbStr), ws);

            Sheet sheet = workbook.getSheet(0);

            int rowCount = sheet.getRows();

            String[][] result = new String[rowCount][];
            for (int i = 0; i < rowCount; i++) {
                Cell[] row = sheet.getRow(i);

                result[i] = new String[row.length];
                for (int j = 0; j < row.length; j++) {
                    result[i][j] = "    " + row[j].getContents();
                }
            }
            return result;

        } catch (Exception e) {
            strHyouji = strHyouji + e.toString();
        } finally {
            if (workbook != null) {
                workbook.close();
            }
        }
        return null;
    }

    public ArrayList<String>[] createList() {
        ArrayList<String>[] listOfList = new ArrayList[16];

        for (int i = 0; i < 16; i++) {
            listOfList[i] = new ArrayList<>();
        }

        return listOfList;
    }

    public ArrayList<String> getList(int position) {
        return priceList[position];
    }

    public String[][] readDiscounts() {
        Workbook workbook = null;
        try {
            WorkbookSettings ws = new WorkbookSettings();
            ws.setGCDisabled(true);

            workbook = Workbook.getWorkbook(new File(dbStr), ws);

            Sheet sheet = workbook.getSheet(0);

            int rowCount = sheet.getRows();

            String[][] result = new String[rowCount][];
            for (int i = 0; i < rowCount; i++) {
                Cell[] row = sheet.getRow(i);

                result[i] = new String[row.length];
                for (int j = 0; j < row.length; j++) {
                    result[i][j] = row[j].getContents();
                }
            }
            return result;

        } catch (Exception e) {
            strHyouji = strHyouji + e.toString();
        } finally {
            if (workbook != null) {
                workbook.close();
            }
        }
        return null;
    }

    public String[][] getArraysDiscount() {
        return arrays;
    }

    public List<MenuEntry> getArraysSettings() {
        return settingsArray;
    }
}
