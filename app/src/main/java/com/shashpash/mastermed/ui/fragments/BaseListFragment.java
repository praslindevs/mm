package com.shashpash.mastermed.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.shashpash.mastermed.R;

import butterknife.BindView;

public abstract class BaseListFragment extends BaseFragment {

    @BindView(R.id.recycler_view)
    protected RecyclerView recyclerView;

    private RecyclerView.Adapter adapter = onCreateAdapter();

    @NonNull
    protected abstract RecyclerView.Adapter onCreateAdapter();

    @NonNull
    protected abstract RecyclerView.LayoutManager onCreateLayoutManager();

    @Override
    protected int getLayout() {
        return R.layout.fragment_list;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(onCreateLayoutManager());
    }

    @Override
    public void onDestroyView() {
        recyclerView.setAdapter(null);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        adapter = null;
    }

    @NonNull
    public final RecyclerView.Adapter getAdapter() {
        return adapter;
    }
}
