package com.shashpash.mastermed.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.shashpash.mastermed.R;

public class SlideActivity extends BaseActivity {

    @Override
    protected int getLayout() {
        return R.layout.activity_slide;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        haveStoragePermission();
    }

    public void click(View view) {
        startActivity(new Intent(this, MenuActivity.class));
    }

    @Override
    public void onBackPressed() {
    }
}
