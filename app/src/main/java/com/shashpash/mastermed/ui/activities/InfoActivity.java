package com.shashpash.mastermed.ui.activities;


import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;
import android.widget.ViewSwitcher;

import com.shashpash.mastermed.Constants;
import com.shashpash.mastermed.R;
import com.shashpash.mastermed.internal.GlideApp;
import com.shashpash.mastermed.model.MenuEntry;

import butterknife.BindView;

public class InfoActivity extends BaseActivity {
    public static final String KEY_MENU_ENTRY = "menuEntry";

    @BindView(R.id.viewSwitcher) ViewSwitcher viewSwitcher;
    @BindView(R.id.videoView) VideoView videoView;
    @BindView(R.id.imageView) ImageView imageView;

    @BindView(R.id.toolbar) Toolbar mActionBarToolbar;

    private MenuEntry menuEntry;
    private int position;

    public static Intent getIntent(Context context, MenuEntry menuEntry, int position) {
        Intent intent = new Intent(context, InfoActivity.class);
        intent.putExtra("position", position);
        intent.putExtra(KEY_MENU_ENTRY, menuEntry);
        return intent;
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_info;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        position = getIntent().getIntExtra("position", 0);
        menuEntry = (MenuEntry) getIntent().getSerializableExtra(KEY_MENU_ENTRY);
        super.onCreate(savedInstanceState);

        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        if (menuEntry.getType() == MenuEntry.Type.VIDEO) {
            viewSwitcher.setDisplayedChild(0);
            initVideoView();
        } else {
            viewSwitcher.setDisplayedChild(1);
            loadImage();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (position < 9)
            getMenuInflater().inflate(R.menu.menu_info, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.price_list_item) {

            startActivity(new Intent(this, PriceActivity.class)
                    .putExtra("position", position));

        } else if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void initVideoView() {

        Point point = new Point();
        getWindowManager().getDefaultDisplay().getSize(point);

        videoView.setVisibility(View.VISIBLE);

        disableSliding();

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                enableSliding();
            }
        });

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {

                        MediaController mediaController = new MediaController(InfoActivity.this);
                        videoView.setMediaController(mediaController);

                        mediaController.setAnchorView(videoView);
                    }
                });
            }
        });

        videoView.setVideoURI(Uri.parse(Constants.PATTERN_URL_DOWNLOAD + menuEntry.getEntryGoogleId()));
        videoView.requestFocus(0);
        videoView.start();
    }

    private void loadImage() {
        GlideApp.with(this)
                .load(Uri.parse(Constants.PATTERN_URL_DOWNLOAD + menuEntry.getEntryGoogleId()))
                .into(imageView);
    }

    private class Callback extends WebViewClient {

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.e("WebViewClient", "onPageFinished");
        }

        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return(false);
        }
    }
}