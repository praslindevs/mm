package com.shashpash.mastermed.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.shashpash.mastermed.R;
import com.shashpash.mastermed.ServiceModel;
import com.shashpash.mastermed.ui.PriceAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shashpash on 21.05.2016.
 */
public class PriceFragment extends Fragment {

    // TODO: switch to one list of data structures
    private ArrayList<String> names;
    private ArrayList<String> prices = new ArrayList<>();

    private ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // TODO: move to base class of fragments
        View view = inflater.inflate(R.layout.fragment_price, null);

        listView = (ListView) view.findViewById(R.id.listView);

        PriceAdapter adapter = new PriceAdapter(getContext(), initData());

        listView.setAdapter(adapter);

        return view;
    }

    private List<ServiceModel> initData() {
        List<ServiceModel> list = new ArrayList<ServiceModel>();
        if (prices.isEmpty()) return list;

        for (int i = 0; i < names.size(); i++) {
            list.add(new ServiceModel(names.get(i), prices.get(i)));
        }

        return list;
    }

    // TODO: move arguments initialization to bundle
    public void setNamesAndPrice(ArrayList<String> names, ArrayList<String> prices) {
        this.names = names;
        this.prices = prices;
    }
}
