package com.shashpash.mastermed.ui.fragments;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;
import com.shashpash.mastermed.Constants;
import com.shashpash.mastermed.R;
import com.shashpash.mastermed.ui.activities.PriceActivity;
import com.shashpash.mastermed.ui.activities.RecyclerViewActivity;

import java.util.List;

import butterknife.BindViews;
import butterknife.OnLongClick;

import static com.shashpash.mastermed.Constants.PASSWORD_KEY;

public class SecondMenuFragment extends BaseMenuFragment {

    @BindViews({
            R.id.tenth_menu_button,
            R.id.eleventh_menu_button,
            R.id.twelveth_menu_button,
            R.id.thirteenth_button,
            R.id.fourteenth_button,
            R.id.fifteenth_button,
            R.id.sixteenth_button,
            R.id.seventeenth_button,
            R.id.eighteenth_button
    })
    protected List<View> items;

    @Override
    protected int getLayout() {
        return R.layout.fragment_menu_second;
    }

    @Override
    protected List<View> getItems() {
        return items;
    }

    @Override
    protected void onItemClick(@NonNull Context context, @IdRes int itemId, int itemPosition) {
        if (itemId == R.id.eighteenth_button && PriceActivity.hasConnection(context)) {
            try {
                Intent sky = new Intent("android.intent.action.VIEW");
                sky.setData(Uri.parse("skype:" + "gl.admin7"));
                startActivity(sky);
            } catch (ActivityNotFoundException ignored) {
                Toast.makeText(context, "Skype не установлен", Toast.LENGTH_SHORT).show();
            }
        } else if (itemId == R.id.seventeenth_button) {

        } else if (itemId == R.id.fourteenth_button) {
            startActivity(new Intent(context, RecyclerViewActivity.class));
        } else {
            startInfoActivity(context, itemPosition + 10);
        }
    }


    @OnLongClick(R.id.seventeenth_button)
    protected boolean showSecurityDialog(final View v) {
        final EditText passwordText =  new EditText(v.getContext());
        new AlertDialog.Builder(v.getContext())
                .setTitle(getString(R.string.enter_password))
                .setView(passwordText)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override public void onClick(DialogInterface dialog, int which) {
                        String password = Hawk.get(PASSWORD_KEY);
                        if (password == null && passwordText.getText().toString().length() >= 4) {
                            Hawk.put(PASSWORD_KEY, passwordText.getText().toString());
                            Toast.makeText(v.getContext(), R.string.the_password_saved_successfully, Toast.LENGTH_SHORT).show();
                        } else if (passwordText.getText().toString().equals(password)) {
                            showSettingsDialog();
                        }
                        dialog.dismiss();
                    }
                })
                .show();

        return true;
    }

    private void showSettingsDialog() {
        final View dialogView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_settings, null);

        final EditText priceUrlEditText = (EditText) dialogView.findViewById(R.id.priceUrlEditText);
        priceUrlEditText.setText(Hawk.get(Constants.PRICE_URL_KEY, Constants.PRICE_URL_DEFAULT));
        final EditText stockUrlEditText = (EditText) dialogView.findViewById(R.id.stockUrlEditText);
        stockUrlEditText.setText(Hawk.get(Constants.STOCK_URL_KEY, Constants.STOCK_URL_DEFAULT));
        final EditText settingsIdEditText = dialogView.findViewById(R.id.settingsIdEditText);
        settingsIdEditText.setText(Hawk.get(Constants.SETTINGS_URL_KEY, Constants.SETTINGS_URL_DEFAULT));

        getBaseActivity().disableSliding();

        new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.settings))
                .setView(dialogView)
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(getString(R.string.save), new DialogInterface.OnClickListener() {
                    @Override public void onClick(DialogInterface dialog, int which) {
                        Hawk.put(Constants.PRICE_URL_KEY, priceUrlEditText.getText().toString());
                        Hawk.put(Constants.STOCK_URL_KEY, stockUrlEditText.getText().toString());
                        Hawk.put(Constants.SETTINGS_URL_KEY, settingsIdEditText.getText().toString());
                        dialog.dismiss();
                        Toast.makeText(dialogView.getContext(), "Настройки сохранены", Toast.LENGTH_SHORT).show();
                    }
                })
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        getBaseActivity().enableSliding();
                    }
                })
                .show();
    }
}
