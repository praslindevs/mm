package com.shashpash.mastermed.ui.fragments;

import android.view.View;

import com.shashpash.mastermed.R;

import java.util.List;

import butterknife.BindViews;

public class MenuFragment extends BaseMenuFragment {

    @BindViews({
            R.id.first_menu_button,
            R.id.second_menu_button,
            R.id.third_menu_button,
            R.id.implantaciya_button,
            R.id.ortodoniya_button,
            R.id.paradontologiya_button,
            R.id.hirurgiya_button,
            R.id.detskaya_stomatologiya_button,
            R.id.price_list_button
    })
    protected List<View> items;

    @Override
    protected int getLayout() {
        return R.layout.fragment_menu;
    }

    @Override
    protected List<View> getItems() {
        return items;
    }
}
