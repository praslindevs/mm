package com.shashpash.mastermed.ui.activities;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;
import com.shashpash.mastermed.Constants;
import com.shashpash.mastermed.DownloadService;
import com.shashpash.mastermed.ParsingExcel;
import com.shashpash.mastermed.R;
import com.shashpash.mastermed.model.MenuEntry;
import com.shashpash.mastermed.ui.fragments.MenuPageFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;

import static com.shashpash.mastermed.ui.activities.PriceActivity.hasConnection;

public class MenuActivity extends BaseActivity implements MenuPageFragment.Callback {

    private Intent intent;
    private ServiceConnection serviceConnection;
    private List<MenuEntry> settings;
    private boolean bound;

    @BindView(R.id.view_pager)
    protected ViewPager viewPager;

    @BindView(R.id.tab_layout)
    protected TabLayout tabLayout;

    @BindViews({R.id.previous, R.id.next})
    protected List<FloatingActionButton> fabs;

    @Override
    protected int getLayout() {
        return R.layout.activity_menu;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ViewPagerAdapterImpl pagerAdapter = new ViewPagerAdapterImpl();

        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                fabs.get(Math.abs(position - 1)).setVisibility(View.VISIBLE);
                fabs.get(position).setVisibility(View.GONE);
            }
        });

        tabLayout.setupWithViewPager(viewPager);

        ParsingExcel parsingExcel = new ParsingExcel("Settings.xls");
        settings = parsingExcel.getArraysSettings();
//        for(MenuEntry menuEntry: settings) {
//            if (menuEntry.getType().equals(MenuEntry.Type.IMAGE)) {
//                intent = new Intent(this, DownloadService.class);
//                intent.putExtra("FileName", menuEntry.getEntryGoogleId() + ".pdf");
//                intent.putExtra("url", Constants.PATTERN_URL_DOWNLOAD + menuEntry.getEntryGoogleId());
//                serviceConnection = new ServiceConnection() {
//                    @Override
//                    public void onServiceConnected(ComponentName name, IBinder service) {
//                        bound = true;
//                    }
//
//                    @Override
//                    public void onServiceDisconnected(ComponentName name) {
//                        bound = false;
//                    }
//                };
//
//                if (haveStoragePermission() && hasConnection(this)) {
//                    bindService(intent, serviceConnection, BIND_AUTO_CREATE);
//                }
//            }
//        }

        if (settings.size() >= 9) {
            pagerAdapter.getItem(0).setup(settings.subList(0, 9), true);
            pagerAdapter.getItem(1).setup(settings.subList(9, settings.size()), false);
        }


        intent = new Intent(this, DownloadService.class);
        intent.putExtra("FileName", "Settings");
        intent.putExtra("url", Constants.PATTERN_URL_DOWNLOAD + Hawk.get(Constants.SETTINGS_URL_KEY,  Constants.SETTINGS_URL_DEFAULT));
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                bound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                bound = false;
            }
        };

        if (haveStoragePermission() && hasConnection(this)) {
            bindService(intent, serviceConnection, BIND_AUTO_CREATE);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (!bound) return;
        unbindService(serviceConnection);
        bound = false;
    }

    @Override
    public void onMenuItemSelected(int position, MenuEntry item) {
        if (position == 16) {
            return;
        } else if (position == 8) {
            startActivity(new Intent(this, PriceActivity.class));
        } else if ( position == 13) {
            startActivity(new Intent(this, RecyclerViewActivity.class));
        } else if (position < 17) {
            startActivity(InfoActivity.getIntent(this, item, position));
        } else {
            try {
                Intent sky = new Intent("android.intent.action.VIEW");
                sky.setData(Uri.parse("skype:" + "gl.admin7"));
                startActivity(sky);
            } catch (ActivityNotFoundException ignored) {
                Toast.makeText(this, "Skype не установлен", Toast.LENGTH_SHORT).show();
            }
        }

        Log.e("MenuActivity", item.toString());
    }

    @OnClick(R.id.next)
    protected void goNext(FloatingActionButton fab) {
        viewPager.setCurrentItem(1);
    }

    @OnClick(R.id.previous)
    protected void goPrevious(FloatingActionButton fab) {
        viewPager.setCurrentItem(0);
    }

    public int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                  int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }


    @Override
    public void onBackPressed() {
        goToSlide();
    }

    private class ViewPagerAdapterImpl extends FragmentPagerAdapter {

        private MenuPageFragment[] pages = new MenuPageFragment[]{
                new MenuPageFragment(),
                new MenuPageFragment()
        };

        private ViewPagerAdapterImpl() {
            super(getSupportFragmentManager());
        }

        @Override
        public MenuPageFragment getItem(int position) {
            return pages[position];
        }

        @Override
        public int getCount() {
            return pages.length;
        }
    }
}
