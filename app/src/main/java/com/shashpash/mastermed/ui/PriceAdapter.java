package com.shashpash.mastermed.ui;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.shashpash.mastermed.ServiceModel;
import com.shashpash.mastermed.R;

import java.util.List;

/**
 * Created by Shashpash on 23.05.2016.
 */
public class PriceAdapter extends BaseAdapter {

    private List<ServiceModel> list;
    private LayoutInflater layoutInflater;

    public PriceAdapter(Context context, List<ServiceModel> list) {
        this.list = list;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_list_view, parent, false);
        }

        ServiceModel serviceModel = getItemObject(position);

        TextView titleTextView = (TextView) view.findViewById(R.id.title_textView),
                priceTextView = (TextView) view.findViewById(R.id.price_textView);

        titleTextView.setText(serviceModel.getTitle());
        priceTextView.setText(serviceModel.getPrice());

        if ((position & 1) == 0) {
            titleTextView.setBackgroundColor(Color.WHITE);
            priceTextView.setBackgroundColor(Color.WHITE);
        }
        return view;
    }

    private ServiceModel getItemObject(int position) {
        return (ServiceModel) getItem(position);
    }

}