package com.shashpash.mastermed.ui.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.HorizontalScrollView;

import com.orhanobut.hawk.Hawk;
import com.shashpash.mastermed.Constants;
import com.shashpash.mastermed.DownloadService;
import com.shashpash.mastermed.ParsingExcel;
import com.shashpash.mastermed.R;
import com.shashpash.mastermed.ui.fragments.PriceFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shashpash on 20.05.2016.
 */
public class PriceActivity extends BaseActivity {
    private TabLayout tabLayout;
    private ServiceConnection serviceConnection;
    private DownloadService downloaderService;
    private boolean bound;
    private Intent intent;


    @Override
    protected int getLayout() {
        return R.layout.activity_price;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final HorizontalScrollView scrollView = (HorizontalScrollView)
                findViewById(R.id.horizontalScrollView);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);

        setupViewPager(viewPager);


        tabLayout = (TabLayout) findViewById(R.id.tabs);
        if (tabLayout != null) {
            tabLayout.setupWithViewPager(viewPager);
        }

        Intent intentVP = getIntent();
        if (intentVP != null) {
            assert viewPager != null;
            viewPager.setCurrentItem(intentVP.getIntExtra("position", 0));
        }

        intent = new Intent(this, DownloadService.class);
        intent.putExtra("FileName", "Price");
        intent.putExtra("url", Constants.PATTERN_URL_DOWNLOAD + Hawk.get(Constants.PRICE_URL_KEY, Constants.PRICE_URL_DEFAULT));
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                downloaderService = ((DownloadService.MyBinder) service).getService();
                bound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                bound = false;
            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();

        startDownload();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (!bound) return;
        unbindService(serviceConnection);
        bound = false;
    }

    private void startDownload() {
        if (haveStoragePermission() && hasConnection(this)) {
            bindService(intent, serviceConnection, BIND_AUTO_CREATE);
        }
    }

    public static boolean hasConnection(final Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        wifiInfo = cm.getActiveNetworkInfo();
        return wifiInfo != null && wifiInfo.isConnected();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            startActivity(new Intent(this, MenuActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupViewPager(ViewPager viewPager) {
        ParsingExcel parsingExcel = new ParsingExcel("Price.xls");

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        PriceFragment fragment1 = new PriceFragment(),
                fragment2 = new PriceFragment(),
                fragment3 = new PriceFragment(),
                fragment4 = new PriceFragment(),
                fragment5 = new PriceFragment(),
                fragment6 = new PriceFragment(),
                fragment7 = new PriceFragment(),
                fragment8 = new PriceFragment();

        int count = 0;
        fragment1.setNamesAndPrice(parsingExcel.getList(count++), parsingExcel.getList(count++));
        adapter.addFragment(fragment1, "Отбеливание зубов");

        fragment2.setNamesAndPrice(parsingExcel.getList(count++), parsingExcel.getList(count++));
        adapter.addFragment(fragment2, "Терапия и профилактика");

        fragment3.setNamesAndPrice(parsingExcel.getList(count++), parsingExcel.getList(count++));
        adapter.addFragment(fragment3, "Ортопедия");

        fragment4.setNamesAndPrice(parsingExcel.getList(count++), parsingExcel.getList(count++));
        adapter.addFragment(fragment4, "Имплантация");

        fragment5.setNamesAndPrice(parsingExcel.getList(count++), parsingExcel.getList(count++));
        adapter.addFragment(fragment5, "Ортодонтия");

        fragment6.setNamesAndPrice(parsingExcel.getList(count++), parsingExcel.getList(count++));
        adapter.addFragment(fragment6, "Хирургия");

        fragment7.setNamesAndPrice(parsingExcel.getList(count++), parsingExcel.getList(count++));
        adapter.addFragment(fragment7, "Парадонтология");

        fragment8.setNamesAndPrice(parsingExcel.getList(count++), parsingExcel.getList(count));
        adapter.addFragment(fragment8, "Детская стоматология");

        viewPager.setAdapter(adapter);
//
//        viewPager.addOnPageChangeListener(
//                new ViewPager.SimpleOnPageChangeListener() {
//
//                    @Override
//                    public void onPageScrolled(int position,
//                                               float positionOffset,
//                                               int positionOffsetPixels) {
//                        Logger.e(this, "onPageScrolled, pos=" + position);
//                    }
//
//                    @Override
//                    public void onPageSelected(int position) {
//                    }
//                }
//        );
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<Fragment>();
        private final List<String> fragmentTitleList = new ArrayList<String>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }
}
