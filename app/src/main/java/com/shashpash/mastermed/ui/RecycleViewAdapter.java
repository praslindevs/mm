package com.shashpash.mastermed.ui;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shashpash.mastermed.R;
import com.shashpash.mastermed.Stock;

import java.util.List;

/**
 * Created by Shashpash on 18.08.2016.
 */
public class RecycleViewAdapter extends  RecyclerView.Adapter<RecycleViewAdapter.StockViewHolder> {

    public static class StockViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView titleTextView;
        TextView textTextView;
        ImageView personPhoto;

        StockViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            titleTextView = (TextView)itemView.findViewById(R.id.person_name);
            textTextView = (TextView)itemView.findViewById(R.id.person_age);
            personPhoto = (ImageView)itemView.findViewById(R.id.person_photo);
        }
    }

    List<Stock> stocks;

    public RecycleViewAdapter(List<Stock> stocks){
        this.stocks = stocks;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public StockViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        return new StockViewHolder(v);
    }

    @Override
    public void onBindViewHolder(StockViewHolder holder, int i) {
        holder.titleTextView.setText(stocks.get(i).getTitle());
        holder.textTextView.setText(stocks.get(i).getText());
        holder.personPhoto.setImageBitmap(stocks.get(i).getPicture());
    }


    @Override
    public int getItemCount() {
        return stocks.size();
    }
}
