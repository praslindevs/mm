package com.shashpash.mastermed.ui.fragments;

import android.content.DialogInterface;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;
import com.shashpash.mastermed.Constants;
import com.shashpash.mastermed.R;
import com.shashpash.mastermed.internal.GlideApp;
import com.shashpash.mastermed.model.MenuEntry;

import java.util.List;

import static com.shashpash.mastermed.Constants.PASSWORD_KEY;

public class MenuPageFragment extends BaseListFragment {

    private static final int ROW_COUNT = 3;

    private List<MenuEntry> entries;

    private int itemHeight;

    private boolean odd;

    @NonNull
    @Override
    protected RecyclerView.Adapter onCreateAdapter() {
        return new AdapterImpl();
    }

    @NonNull
    @Override
    protected RecyclerView.LayoutManager onCreateLayoutManager() {
        return new GridLayoutManager(
                getContext(),
                ROW_COUNT,
                GridLayoutManager.VERTICAL,
                false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Point point = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(point);
        itemHeight = point.y / ROW_COUNT;
    }

    public void setup(List<MenuEntry> entries, boolean odd) {
        this.entries = entries.size() < 9 ? entries : entries.subList(0, 9);
        this.odd = odd;
        getAdapter().notifyDataSetChanged();
    }

    private Callback getCallback() {
        return (Callback) getActivity();
    }

    private class AdapterImpl extends RecyclerView.Adapter<AdapterImpl.ViewHolder> {

        private AdapterImpl() {
            setHasStableIds(true);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_menu_entry,
                            parent,
                            false);

            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    itemHeight);

            v.setLayoutParams(layoutParams);

            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.bind(entries.get(position), position % 2 == (odd ? 0 : 1));

            holder.title.setVisibility((!odd && position == 7) ? View.INVISIBLE : View.VISIBLE);
            holder.icon.setVisibility((!odd && position == 7) ? View.INVISIBLE : View.VISIBLE);
        }

        @Override
        public int getItemCount() {
            return entries == null ? 0 : entries.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            private final TextView title;
            private final ImageView icon;

            private ViewHolder(View itemView) {
                super(itemView);

                title = itemView.findViewById(R.id.title);
                icon = itemView.findViewById(R.id.icon);
            }

            private void bind(MenuEntry entry, final boolean highlighted) {
                title.setText(entry.getTitle());

                int id = highlighted ? R.color.light_blue : R.color.white;
                int color = ContextCompat.getColor(itemView.getContext(), id);
                itemView.setBackgroundColor(color);

                GlideApp.with(title.getContext())
                        .load(entry.getImageUrl())
                        .into(icon);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int position = getAdapterPosition();

                        getCallback().onMenuItemSelected(
                                position + (odd ? 0 : 9),
                                entries.get(position));

                    }
                });

                itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {

                        if (!odd && getAdapterPosition() == 7) {

                            final EditText passwordText = new EditText(v.getContext());
                            new AlertDialog.Builder(v.getContext())
                                    .setTitle(getString(R.string.enter_password))
                                    .setView(passwordText)
                                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            String password = Hawk.get(PASSWORD_KEY);
                                            if (password == null && passwordText.getText().toString().length() >= 4) {
                                                Hawk.put(PASSWORD_KEY, passwordText.getText().toString());
                                                Toast.makeText(itemView.getContext(), R.string.the_password_saved_successfully, Toast.LENGTH_SHORT).show();
                                            } else if (passwordText.getText().toString().equals(password)) {
                                                showSettingsDialog();
                                            }
                                            dialog.dismiss();
                                        }
                                    })
                                    .show();

                        }

                        return true;
                    }
                });
            }
        }
    }

    private void showSettingsDialog() {
        final View dialogView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_settings, null);

        final EditText priceUrlEditText = (EditText) dialogView.findViewById(R.id.priceUrlEditText);
        priceUrlEditText.setText(Hawk.get(Constants.PRICE_URL_KEY, Constants.PRICE_URL_DEFAULT));
        final EditText stockUrlEditText = (EditText) dialogView.findViewById(R.id.stockUrlEditText);
        stockUrlEditText.setText(Hawk.get(Constants.STOCK_URL_KEY, Constants.STOCK_URL_DEFAULT));
        final EditText settingsIdEditText = dialogView.findViewById(R.id.settingsIdEditText);
        settingsIdEditText.setText(Hawk.get(Constants.SETTINGS_URL_KEY, Constants.SETTINGS_URL_DEFAULT));

        getBaseActivity().disableSliding();

        new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.settings))
                .setView(dialogView)
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(getString(R.string.save), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Hawk.put(Constants.PRICE_URL_KEY, priceUrlEditText.getText().toString());
                        Hawk.put(Constants.STOCK_URL_KEY, stockUrlEditText.getText().toString());
                        Hawk.put(Constants.SETTINGS_URL_KEY, settingsIdEditText.getText().toString());
                        dialog.dismiss();
                        Toast.makeText(dialogView.getContext(), "Настройки сохранены", Toast.LENGTH_SHORT).show();
                    }
                })
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        getBaseActivity().enableSliding();
                    }
                })
                .show();
    }

    public interface Callback {

        void onMenuItemSelected(int position, MenuEntry item);
    }
}
