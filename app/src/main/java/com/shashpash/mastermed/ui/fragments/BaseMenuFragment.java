package com.shashpash.mastermed.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.shashpash.mastermed.ui.activities.InfoActivity;
import com.shashpash.mastermed.ui.activities.PriceActivity;

import java.util.List;

public abstract class BaseMenuFragment extends BaseFragment implements View.OnClickListener {

    protected abstract List<View> getItems();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        for (View v : getItems()) {
            v.setOnClickListener(this);
        }
    }

    @Override
    public final void onClick(View view) {
        onItemClick(
                view.getContext(),
                view.getId(),
                getItems().indexOf(view));
    }

    protected void onItemClick(@NonNull Context context, @IdRes int itemId, int itemPosition) {
        Log.e("Shashpash", "onItemClick: " + itemPosition);
        if (itemPosition == 8) startActivity(new Intent(context, PriceActivity.class));
        else startInfoActivity(context, itemPosition + 1);
    }

    protected final void startInfoActivity(Context context, int position) {

        Intent intent =
                new Intent(context, InfoActivity.class).putExtra("position", position);

        startActivity(intent);
    }
}
