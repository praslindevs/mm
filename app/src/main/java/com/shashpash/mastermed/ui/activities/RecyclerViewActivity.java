package com.shashpash.mastermed.ui.activities;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.orhanobut.hawk.Hawk;
import com.shashpash.mastermed.Constants;
import com.shashpash.mastermed.DownloadService;
import com.shashpash.mastermed.ParsingExcel;
import com.shashpash.mastermed.R;
import com.shashpash.mastermed.Stock;
import com.shashpash.mastermed.ui.RecycleViewAdapter;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class RecyclerViewActivity extends BaseActivity {
    private List<Stock> stocks;
    private RecyclerView recyclerView;

    private boolean bound;
    private ServiceConnection serviceConnection;
    private DownloadService downloaderService;
    private Intent intent;

    @Override
    protected int getLayout() {
        return R.layout.recyclerview_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        recyclerView = (RecyclerView) findViewById(R.id.rv);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initializeData();

        //initializeAdapter();

        intent = new Intent(this, DownloadService.class);
        intent.putExtra("FileName", "Discounts");
        intent.putExtra("url", Constants.PATTERN_URL_DOWNLOAD + Hawk.get(Constants.STOCK_URL_KEY, Constants.STOCK_URL_DEFAULT));
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                downloaderService = ((DownloadService.MyBinder) service).getService();
                bound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                bound = false;
            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();

        startDownload();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (!bound) return;
        unbindService(serviceConnection);
        bound = false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void startDownload() {
        if (haveStoragePermission() && PriceActivity.hasConnection(this)) {
            bindService(intent, serviceConnection, BIND_AUTO_CREATE);
        }
    }

    private void initializeData() {
        ParsingExcel parsingExcel = new ParsingExcel("Discounts.xls");
        final String[][] arrays = parsingExcel.getArraysDiscount();
        stocks = new ArrayList<>();

        if (arrays != null) {
//        DownloadData downloadData = new DownloadData(arrays);
//        downloadData.execute();

            Thread thread;
            if (haveStoragePermission() && PriceActivity.hasConnection(this)) {
                thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (final String[] array : arrays) {
                            if (array != null) {

                                int index = 0;
                                if (!array[index].equals("")) {
                                    try {
                                        stocks.add(new Stock(array[index++], array[index++],
                                                //BitmapFactory.decodeResource(getResources(), R.drawable.a_a)));
                                                BitmapFactory.decodeStream(new URL(array[index]).openConnection().getInputStream())));
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }

                            }
                        }
                    }

                });
            } else {
                thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (final String[] array : arrays) {
                            if (array != null) {
                                int index = 0;
                                if (!array[index].equals("")) {
                                    stocks.add(new Stock(array[index++], array[index],
                                            BitmapFactory.decodeResource(getResources(), R.drawable.r)));

                                }

                            }
                        }
                    }

                });
            }

            thread.start();
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        initializeAdapter();

    }


    private void initializeAdapter() {
        RecycleViewAdapter adapter = new RecycleViewAdapter(stocks);
        recyclerView.setAdapter(adapter);
    }

//    class DownloadData extends AsyncTask<Void, Void, List<Stock>> {
//        String[][] arrays;
//
//        public DownloadData(String[][] arrays) {
//            this.arrays = arrays;
//        }
//
//        @Override
//        protected List<Stock> doInBackground(Void... params) {
//            for (final String[] array : arrays) {
//                if (array != null) {
//
//                    int index = 0;
//                    Logger.e(this, "new Thread(new Runnable() {");
//                    if (!array[index].equals("")) {
//                      //  try {
//                            stocks.add(new Stock(array[index++], array[index++],
//                                    BitmapFactory.decodeResource(getResources(), R.drawable.a_a)));
//                                    //BitmapFactory.decodeStream(new URL(array[index]).openConnection().getInputStream())));
////                        } catch (IOException e) {
////                            e.printStackTrace();
////                        }
//                    }
//
//                }
//            }
//            return stocks;
//        }
//
//        @Override
//        protected void onPostExecute(List<Stock> stocks) {
//            super.onPostExecute(stocks);
//            initializeAdapter();
//        }
//    }

}
