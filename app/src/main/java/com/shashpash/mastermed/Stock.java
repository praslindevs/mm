package com.shashpash.mastermed;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Created by Shashpash on 18.08.2016.
 */
public class Stock {
    private String title;
    private String text;
    private Bitmap picture;

    public Stock(String title, String text, Bitmap picture) {
        this.title = title;
        this.text = text;
        this.picture = picture;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public Bitmap getPicture() {
        return picture;
    }
}
