package com.shashpash.mastermed.model;

import com.shashpash.mastermed.Constants;

import java.io.Serializable;

public class MenuEntry implements Serializable{
    private String title;
    private String imageUrl;
    private Type type;
    private String entryGoogleId;

    public static MenuEntry fromStringArray(String[] menuEntry) {
        MenuEntry instance = new MenuEntry();
        instance.title = menuEntry[0];
        instance.imageUrl = Constants.PATTERN_URL_DOWNLOAD + menuEntry[1];
        if (!menuEntry[2].equals("")) {
            instance.type = Type.VIDEO;
            instance.entryGoogleId = menuEntry[2];
        } else {
            instance.type = Type.IMAGE;
            instance.entryGoogleId = menuEntry[3];
        }
        return instance;
    }
    public String getTitle() {
        return title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Type getType() {
        return type;
    }

    public String getEntryGoogleId() {
        return entryGoogleId;
    }

    public enum Type {
        VIDEO,
        IMAGE
    }

    @Override
    public String toString() {
        return "MenuEntry{" +
                "title='" + title + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", type=" + type +
                ", entryGoogleId='" + entryGoogleId + '\'' +
                '}';
    }
}
