package com.shashpash.mastermed;


import android.os.Environment;

public class Constants {
    public static final String MASTER_MED_DIRECTORY = Environment.getExternalStorageDirectory() + "/MasterMed/";

    public static final String MP4_TYPE = ".mp4";

    public static final String PDF_TYPE = ".pdf";

    public static final String PASSWORD_KEY = "passwordKey";

    public static final String PRICE_URL_KEY = "priceUrlKey";

    public static final String STOCK_URL_KEY = "stockUrlKey";

    public static final String SETTINGS_URL_KEY = "settingsUrlKey";

    public static final String SETTINGS_URL_DEFAULT = "1klbxGA1i-_AvMrAxgitwzPvK3XGmtZ-J";

    public static final String PATTERN_URL_DOWNLOAD = "https://drive.google.com/uc?export=download&id=";

    public static final String PRICE_URL_DEFAULT = "1l-kdqZ4St6pCu9PcYkedz2jWRs8nufPb";

    public static final String STOCK_URL_DEFAULT = "115j3xCu8cjo8iv91qdrz89pr64ptVU7n";
}
